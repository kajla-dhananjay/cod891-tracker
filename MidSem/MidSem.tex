\documentclass{beamer}
\usepackage[utf8]{inputenc}

\usepackage{amsmath, tcolorbox, hyperref, bm} %font utopia imported
\hypersetup{
	colorlinks=true,
	linkcolor=blue,
	filecolor=magenta,      
	urlcolor=cyan,
}
\usetheme{Madrid}
\usecolortheme{default}
\title{COD891 Mid-Sem Presentation}
\author{Dhananjay Kajla}
\date{March 23, 2021}
\begin{document}
	\frame{\titlepage}
	\begin{frame}
		\frametitle{Table of Contents}
		\tableofcontents
	\end{frame}
	
	\section{Introduction}
	
	\begin{frame}
		\frametitle{Introduction}
		\begin{itemize}
			\item Project Title : {\bf A Laplacian Solver using Random Walks}
			
			\item Course : COD891
			
			\item Duration : 12-14 weeks
			
			\pause
			
			\item Goal : To improve upon the paper {\sc A Distributed Laplacian Solver}\textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}} and reduce the worst case complexity of finding the solution to Graph-Laplacian using random walks.
			
			\item Motivation : 
			\begin{itemize}
				\item The original paper had a time complexity of ${\Omega}(n)$ and can be very high, even quadratic in some cases. 
				\item This project is based on the idea that we should be able to modify the algorithm to achieve a better worst case complexity.
			\end{itemize}
			%\begin{itemize}
			%	\item The time complexity for the original algorithm was $\tilde{O}(t_{hit})$.
			%	\item Here $t_{hit}$ is the hitting time of the graph and is $$
			%\end{itemize}
		\end{itemize}
	\end{frame}
	
	\section{The Laplacian Problem} 
	
	\begin{frame}
		\frametitle{The Laplacian of a graph}
		\begin{itemize}
			\item Let $G(V,E,w)$ be an undirected, connected, weighted graph, where $w : E \rightarrow \mathbb{R}^{+}$ is a weight function on the edges, $n = |V|$ is the number of vertices and $m = |E|$ is the number of edges. 
			\pause
			\item We define two matrices ($A$, $D$) based on the graph($G$) and we use $\delta_i$ to denote the degree of vertex $i$. Their entries are given by : 
			\begin{itemize}
				\item $A_{i,j} = \begin{cases}
					w(e) \text{ if }e = (i,j) \in E, \\
					0 \text{ otherwise }
				\end{cases}$
				\item $D_{i,j} = \begin{cases}
					\sum_{v:i\sim v}w((i,v)) \text{ if } i = j, \\
					0 \text{ otherwise }
				\end{cases}$
			\end{itemize}
			\pause
			\item \hypertarget{lap}{Now we can define the Laplacian Matrix of the given graph G.}
			\[L = D - A\]
		\end{itemize}
	\end{frame}
	\begin{frame}
		\frametitle{Lx = b}
		\begin{itemize}
			\item Now we are ready to introduce the problem. 
			\pause
			\item Given a connected, undirected and weighted graph $G(V,E,w)$ and ${\bm b } \in \mathbb{R}^{n}$, we have 
			\[\text{Find } {\bm{x}} \in \mathbb{R}^{n} \text{, such that } {\mathbf{L}\bm{x}} = {\bm{b}}\]
			\item Finding Such $\bm{x}$ for a given graph $G$ and vector $\bm{b}$ is the problem we shall try to solve.
			\pause 
			\item In fact we shall only be interested in a very special case of this problem where only a single element of $\forall i \in [n-1], {b}_i \geq 0$  and $\sum_{i=1}^{n}({{b}_i}) = 0$.
		\end{itemize}
	\end{frame}
	
	\section{Laplacian and Random Walks}
	\begin{frame}
		\frametitle{Motivation for current algorithm}
		\begin{itemize}
			\item Our algorithm is a modified version of {\sc A Distributed Laplacian Solver}\textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}}
			\item The time complexity for that paper was $\tilde{O}(t_{hit} \cdot d_{max})$.
			\pause 
			\item Here $d_{max}$ is the generalized maximum degree of the graph. (Maximum entry in the diagonal of L).
			\item Here $t_{hit}$ is the hitting time of the graph and is always $\Omega(n)$ but is usually $\Theta(n)$ in most graphs.
			\item However $t_{hit}$ can rise as high as $\Theta(n^2)$ for certain graphs.
			\item The motivation for current algorithm was to try to replace the infinite packet data collection process they used into a finite one so as to get a better worst case time complexity.
			%\item Given a graph $G(V,E,w)$ with $|V| = n$ and it's corresponding \hyperlink{lap}{Laplacian} $L$, and a vector $\bm{b} \in R^{n}$ such that $\forall i \in [n-1], {\bm b}_i \geq 0$ and $\sum_{i = 1}^{i = n}{\bm b}_i = 0$, find ${\bm x} \in \mathbb{R}^{n}$ such that $L{\bm x} = {\bm b}$.
			%\pause 
			%\item Due to special property of {\bf b}, we take the convention to refer to this special case as "one-sink" Laplacian systems. 
		\end{itemize}
	\end{frame}
	\begin{frame}
		\frametitle{A broad overview of the algorithm}
		\begin{itemize}
		%	\item To solve the Laplacian, our algorithm essentially defines a discrete time multi-dimensional Markov chain $\{Q_{t}^{\beta}\}$. Here $\beta$ is a parameter.
			
			\item Our algorithm, like the previous paper\textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}}, solves the Laplacian for a special, yet important case where $\sum_{i=1}^{i=n}{b_{i}} = 0$ and there exists a unique $i \in [n]$ s.t. $b_{i} < 0$.
			
			\item We call this a "one-sink" Laplacian system.
			\pause 
			\item For our algorithm, we shall construct the graph corresponding to the Laplacian(L). 
			\pause 
			\item We will then find a sink node $u_s$. This node collects the packets and redistributes them to all the other nodes with probabilities given by a relative rate vector ${\bm J}$ such that ${\bm J} \cdot \text{\bf 1} = 0$. 
			\item More on ${\bm J}$ later.
			\pause 
			\item \hypertarget{tmat}{Now we shall soon define a Markov Chain based on the graph with transition matrix } :  
			\[P_{u,v} = \begin{cases}\frac{w((u,v))}{\sum_{v:i\sim v} w((i,v))} , u \neq u_{s} \\ {\bm J}_v , u = u_s \end{cases}\] 
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Laplacian solver using Random Walks}
		\begin{itemize}
			\item Let us now define a discrete time multi-dimensional Markov Chain $\{Q_{t}\}$.
			\item For any node $ v \in V \setminus \{u_s\}$, $Q_{t}(v)$ denotes the number of packets at vertex $v$ at time $t$.
			\pause 
			\item Now let us define our random walk : 
			\pause 
			\item At each time step $(t \geq 0)$, every node $u \in V \setminus \{u_s\}$ with non-empty queue picks a packet (uniformly at random), it transfers this packet to it's neighbors using the previously defined \hyperlink{tmat}{transition matrix}. 
			\item Our sink $u_s$ does not hold packets, it instantly teleports all packets it receives to all other vertices with probability ${\bf J}$. That is, a packet which reached $u_s$ will end up at a vertex $v \neq u_s$ with probability $J_v$.
		\end{itemize}
	\end{frame}
	
	\begin{frame}
		\frametitle{Relation of given Random Walk to Laplacian}
		\begin{itemize}
			\item Modified from \textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}}
			\item Consider the expectation for packet queue size for a single step of our random walk for any node $v \in V \setminus \{u_s\}$ :  \\
			\[E[Q_{t+1}(u) | Q_{t}(u)] = t_1 + t_2 + t_3 + t_4\]
			\pause 
			\item Where : \\
			\begin{itemize}
				\item $t_1 =  Q_{t}(u)$ is the number of packets at $u$ at time $t$.
				\item $t_2 = - \text{\bf 1 }_{\{Q_{t}(u) > 0\}} \sum_{v : v \sim u} P[u,v]$ is the number of packets it sends to it's neighbors.
				\item $t_3 = \sum_{v : v \sim u} P[u,v] \text{\bf 1 }_{\{Q_{t}(v) > 0\}} $ is the number of packets it receives from it's neighbors.
				\item $t_4 = T_t(u)$ is the number of packets teleported from the sink.
			\end{itemize}
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Relation of given Random walk to Laplacian contd.}
		\begin{itemize}
			\item Let $\eta_{u}^{t} = P[Q_{t}(u) > 0]$ be the occupancy probability at node $u$. 
			\item Observe that by definition of ${\bm J}$, $E[T_{t}(u)] = {\bm J}_{u}$
			\item Now taking expectation on both sides of the previous equation : 
			\[E[Q_{t+1}(u)] = E[t_1] + E[t_2] + E[t_3] + E[t_4]\]
			\pause 
			\item That is,
			\[E[Q_{t+1}(u)] = E[Q_{t}(u)] - \eta_{u}^{t} \sum_{v : v \sim u} P[u,v] + \sum_{v : v \sim u} P[v,u] \eta_{v}^{t} + {\bm J}_{u} \]
			\pause 
			\item At stationary state, $E[Q_{t+1}(u)] = E[Q_{t}(u)]$ and let ${\bm \eta}_{u}$ be the occupancy probability.
			\pause 
			\item Therefore,
			\[ - {\bm \eta}_{u} \sum_{v : v \sim u} P[u,v] + \sum_{v : v \sim u} P[v,u] {\bm \eta}_{v} + {\bm J}_{u} = 0\]
		\end{itemize}
	\end{frame}

	
	\begin{frame}
		\frametitle{Relation of given Random walk to Laplacian contd.}
		\begin{itemize}
			\item Rewriting the previous equation in vector form\textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}} :
			\[{\bm \eta}^{T}(I - P ) = {\bm J}^{T}\]
			\item Here P is the \hyperlink{tmat}{transition matrix} for our Markov Chain.
			\pause 
			\item Now $P = D^{-1}A$. Now this isn't quite true here, for we have changed the transition matrix for the sink vertex.
			\item However the difference only comes at the row corresponding to $u_s$. This can be easily computed later once the other values have been computed.
			\pause 
			\item Therefore, setting ${\bm x}^{T} = {\bm \eta}^{T}D^{-1}$ 
			\[{\bm x}^{T}L =  {\bm J}^{T}\]
			\pause 
			\item Note that $D$ is a diagonal matrix, therefore computing it's inverse is trivial. 
			\item Hence we have established how our Random Walk is capable of solving the Laplacian.
		\end{itemize}
	\end{frame}

	\section{Current Work}
	\begin{frame}
		\frametitle{Laplacian solver using Random Walks Algorithm}
		 Now Let us state our algorithm in steps (modified from \textsuperscript{\href{https://arxiv.org/abs/1905.04989}{[1]}}) :
		
		\begin{enumerate}
			\item Create Network $G = (V, E, w)$ from the corresponding Laplacian $L$.
			\pause 
			\item Since ${\bm b}_v < 0$ for a single $v$, make that $v = u_s$ the sink for the process. 
			\item Given $u_s$. Let ${\bm J} = \dfrac{1}{\sum_{i = 1}^{i = n}(b_i) - b_s } \cdot {\bm b}$
			\pause 
			\item Add directed edges from $u_s$ to all other nodes with weights, $w(u_s,v) = {\bm J}_v$ so that the markov chain has the given \hyperlink{tmat}{transition matrix}.
			\pause 
			\item Find stationary distribution of this Modified markov chain $\tilde{\pi}$.
			\item Place $k$ data packets on the vertices of the graph with distribution ${\bm{\tilde{\pi}}}$. That is pick a packet, place it at vertex $v$ with probability ${\tilde{\pi}}(v)$.
			\pause 
			\item Let the chain mix and using our random walk compute ${\bm \eta}$, the occupancy vector.
			\item This solves the given Laplacian. 
		\end{enumerate}
	\end{frame}


	\begin{frame}
		\frametitle{Current Work}
	 The current work we are doing can be divided into two parts : 
	 \medskip
		\begin{itemize}
			\item Part 1 - Getting the Markov Chain to Stationary. (i.e. computing $\tilde{\pi}$)
			\medskip
			\item Part 2 - Computing the occupancy probabilities.
		\end{itemize}
	\end{frame}
	
	\begin{frame}
		\frametitle{Current Work, Part - 1 : Approach I}
		\begin{itemize}
			\item For part 1, we have explored various ways to solve the problem of computing $\tilde{\pi}$ efficiently.
			\item The problem stems from the fact that computation of stationary probability of states with very low probability seems infeasible. 
			\item To compute the stationary probability of a state with very low stationary probability, it makes sense that one would need to do a lot of sampling so as to get an accurate probability. This problem would lead us to inefficiency.
			\item That is, Markov Chain Monte Carlo methods or Random walks (such as ours) require $\tilde{O}(\frac{\tau}{\tilde{\pi}(v)})$ time to approximate the steady state probability $\tilde{\pi}(v)$.
			\item This ofcourse becomes a huge barrier for states with $\tilde{\pi}(v) = o(\frac{1}{n})$. That is, if the stationary probability of a node is sub-linear, then the time required for an accurate computation becomes super-linear.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Current Work, Part - 1 : Approach I contd.}
		\begin{itemize}
			\item To solve this problem, a possible solution was proposed by Bressan\textsuperscript{\href{https://arxiv.org/pdf/1801.00196.pdf}{[5]}}.
			\item Unfortunately their solution pertains only to time reversible markov chain which is not the case here.
			\pause 
			\item They also give an impossibility proof for a family of time-irreversible markov chains in which they claim that no algorithm can compute $\tilde{\pi}$ in less that $\frac{\tau}{\tilde{\pi}(v)}$ time.
			\item The family they propose is  quite distinct from our case, however it raises some doubts as to whether it might be the case that it just might not be possible to compute it efficiently through this method.
			\item Therefore being agnostic on this approach, we pivoted to another approach : {\bf Preprocessing}.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\frametitle{Current Work, Part - 1 : Approach II}
		\begin{itemize}
			\item Currently we are working on a pre-processing based approach for a Laplacian solver.
			\item The idea is to sample some N random walks for a given graph and use various data-structures to amortize the cost of computing the solution.
			\item While this will likely result in some very costly computations, however the benefit we derive is that the computations work for any {\bf b} which satisfies the "one sink" property.

		\end{itemize}
	\end{frame}
	
	\begin{frame}
		\frametitle{Current Work, Part - I : Approach II : Specification}
	\end{frame}

	\begin{frame}
		\frametitle{Current Work, Part - I : Approach II : Analysis}
	\end{frame}
	
	\begin{frame}
		\frametitle{Current Work, Part - II : Readings and Ideas}
		\begin{itemize}
			\item Once we have solved the problem of getting the Markov chain to stationary distribution, we shall have to find occupancy probabilities.
			\item Proofs of this part seem to require mathematical knowledge and understanding in the field of stable multivariate polynomials.
			\item Regarding this, we have spent 4 weeks reading various parts of the survey of the field of multivariate stable polynomials by David Wagner\textsuperscript{\href{https://arxiv.org/abs/0911.3569}{[6]}}
			\item With Part - I almost complete, we shall begin work on Part - II of the problem. Computing the occupancy probabilities and relating them to the Laplacian Itself. 
		\end{itemize}
	\end{frame}

	\section{Readings and Future Work}	
	\begin{frame}
		\frametitle{Surveys and Papers Read}
		As a part of this project, the following papers (among others) have been read in various proportions, so as to either get better acquainted with or to try to solve the problem.
		\begin{itemize}
			\item David Wagner's \href{https://arxiv.org/pdf/0911.3569.pdf}{Survey} : Read extensively for proofs that shall come in use in part 2.
			\item Levin, Peres' \href{https://pages.uoregon.edu/dlevin/MARKOV/markovmixing.pdf}{Markov Chains and Mixing Times} : Chapter 1, 2, 9, 10. So as to better understand Markov Chains.
			\item \href{https://arxiv.org/pdf/1603.07796.pdf}{Approximate Personalized Page Rank on Dynamic Graphs} : Read so as to get some inspiration on how to try to approximate steady state probability for our non reversible Markov chain, starting from a reversible one by dynamically modifying the graph.
			\item \href{https://arxiv.org/pdf/1006.2880.pdf}{Fast, Incremental and Personalized PageRank} : So as to get inspiration on amortization and pre-processing.
			\item \href{https://arxiv.org/pdf/1801.00196.pdf}{On approximating stationary distribution of time-reversible markov chains} : So as to give some motivation on whether to tackle the $\frac{\tau}{\pi(v)}$ problem or to go to a new approach.
			\item Other papers and references when required.
		\end{itemize}
	\end{frame}
	
\end{document}