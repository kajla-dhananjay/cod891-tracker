# COD891 Tracker
---

This repository is essentially the weekly (and daily) progress logs as well as my work for COD891 Sem ii - 2020-21. The purpose of this repository is to keep help organize the project and keep a personal(and public) track of progress.

---

## Files and Directories 

---

- Calendar.md : Personal logs of the daily time spent on the project and the progress achieved for the day.

- Issues.md : Logs the issues I faced in the readings and their status. 

- Milestones.md : Weekly milestones and tracking their progress. 

- Wagner_Elaborated : This directory will contain my personal explaination/proof of the propositions, lemma's and excercises of Wagner's <a href="http://www.math.uwaterloo.ca/~dgwagner/ceb_wagner.pdf">Survey</a>. 
